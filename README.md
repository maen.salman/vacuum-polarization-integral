# Vacuum polarization integral
**Description**
This Mathematica program computes the integral (along the Feynman path) associated with the vacuum polarization correction for arbitrary numbers of potential interactions. 

**Input:**
The input of this program is a vector containing energy levels (poles of the Green's function).

**Output:**
The program will compute the integral using the derived analytical result (fast), and compare it with the complex integration using MATHEMATICA (SLOW)
